<?php
class Cipher{
	private $key;
	
	public function __construct( $key ){
		$this->key = $key;
	}
	
	public function encrypt( $input ){
		/*creamos un identificador de encriptado en el que indicamos
		el tipo de cifrador (cast-128) y el modo de cifrado (ecb) */
		$ident = mcrypt_module_open('cast-128', '', 'ecb', '');
		/* dado que algunas funciones requieren de un vector de inicializacion
		acorde con sus especificaciones esta función determina el tamaño
		de ese vector atendiendo al tipo de identificador */
		$long_iniciador=mcrypt_enc_get_iv_size($ident);
		/* crea el vector de inicialización con valores aleatorios
		y dándole la dimensión precalculada en la función anterior */
		$inicializador = mcrypt_create_iv ($long_iniciador, MCRYPT_RAND);
		
		/* Contimuamos la secuencia de encriptado incializando todos los buffer
		necesarios para llevar a cabo las labores de encriptado */
		mcrypt_generic_init($ident, $this->key, $inicializador);
		
		$texto_encriptado = mcrypt_generic($ident, $input);
		
		/* libera los buffer pero no cierra el modulo */
		mcrypt_generic_deinit($ident);
		
		/* esta instruccion es necesaria para cerrar el modulo de encriptado*/
		mcrypt_module_close($ident);

		return base64_encode( $texto_encriptado );
	}
	
	public function decrypt( $input ){
		/* leemos el fichero encriptado creado por el script anterior */
		$texto_encriptado = base64_decode( $input );
		
		/*creamos un identificador de encriptado que ha de ser el mismo con
		el que hemos realizado la encriptación */
		$ident = mcrypt_module_open('cast-128', '', 'ecb', '');

		/* dado que algunas funciones requieren de un vector de inicializacion
		acorde con sus especificaciones esta función determina el tamaño de ese
		vector atendiendo al tipo de identificador anterior*/
		$long_iniciador=mcrypt_enc_get_iv_size($ident);
		
		/* crea el vector de inicialización con valores aleatorios
		y dándole la dimensión precalculada en la función anterior */
		$inicializador = mcrypt_create_iv ($long_iniciador, MCRYPT_RAND);
		
		/* incializa todos los buffer necesarios para llevar
		a cabo las labores de encriptado */
		mcrypt_generic_init($ident, $this->key, $inicializador);
		
		/* realiza el desencriptado proopiamente dicho. Realmente es la unica
		diferencia básica entre este script y el ejemplo anterior */
		$desencriptado = mdecrypt_generic($ident, $texto_encriptado); 
		
		/* libera los buffer pero no cierra el modulo */
		mcrypt_generic_deinit($ident);
		
		/* esta instruccion es necesaria para cerrar el modulo de encriptado*/
		mcrypt_module_close($ident);

		return trim( $desencriptado );
	}
}
/*
$a = new Cipher('2316549875689654');
echo "<br><br>";

echo "lania '{$a->encrypt('lania')}'<br><br>"; 
echo "itver '{$a->encrypt('itver')}'<br><br>"; 

echo "lania '{$a->decrypt('OgddbrcL/K0=')}'<br><br>"; 
echo "itver '{$a->decrypt('0uVdcahgOGE=')}'<br><br>"; 

echo "lania" == "lania";
 
echo "lania" == trim($a->decrypt("OgddbrcL/K0="));
*/
