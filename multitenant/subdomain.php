<?php
require_once('cipher.php');

function get_subdomain()
{
    $subdomain_arr = explode('.', $_SERVER['HTTP_HOST'], 2);
    $subdomain_name = $subdomain_arr[0];
    return $subdomain_name;
}

# Deberiamos checar que el subdominio este como parte de las instituciones registradas.
GLOBAL $CFG;
$CFG->subdomain = get_subdomain();

# Está es una llave que debe de estar en un archivo
$CFG->cipher = new Cipher( '2316549875689654' );
